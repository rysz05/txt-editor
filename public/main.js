
function format(command, value) {
	document.execCommand(command, false, value);
}

let fileName = null;
let filesList = document.getElementById("savedFiles")
let message = document.getElementById("savedFiles_title")

let showed = false


function cleanUp() {
	filesList.innerHTML = "";
	message.innerHTML = ""
	showed = false;
}

function getInput() {

	var contenteditable = document.querySelector('[contenteditable]')
	let text = contenteditable.innerHTML

	    let file_name = prompt("Save as...", "myFile");
	    if (file_name != null) {
		    let json = {
		    	name: file_name + '.json',
		    	text: text
		    }
		    cleanUp()
		return json
}}

async function save() {

	let json = getInput()
	let res = await axios.post("./saveInput", json)
	if (res.status === 200) {
		message.innerHTML = "File saved"
	}

	cleanUp();
}

async function showSavedFiles() {

	if(showed === false) {
		let savedFiles = await axios.get("./getSavedFiles")
		savedFiles = savedFiles.data
		if (savedFiles.length) {
		showed = true;
			message.innerHTML = "Saved files: "
			for (let i in savedFiles) {
				let li = document.createElement("LI")
				li.setAttribute('onclick', `loadFile('${savedFiles[i]}')`)
				li.setAttribute('class', 'file_list_el')
				li.innerHTML = savedFiles[i]
				filesList.appendChild(li)

			}
		} else {
			message.innerHTML = "No saved files."
		}
	}
}

async function loadFile(fileToLoad) {

	cleanUp();

	let fileValue = await axios.get(`./loadFile/${fileToLoad}`)
	document.querySelector('[contenteditable]').innerHTML = fileValue.data.text
	//fileContent = fileValue.data.text
	fileName = fileToLoad
}

async function clearAllSaved() {

	cleanUp();
	let res = await axios.get('./deleteFiles')
	// console.log(res)
	if (res.status === 200) {
		message.innerHTML = "Files deleted"
	}
}

// DOWNLOAD FILE TO COMPUTER

const downloadToFile = (content, filename, contentType) => {
  const a = document.createElement('a');
  const file = new Blob([content], {type: contentType});

  a.href= URL.createObjectURL(file);
  a.download = filename;
  a.click();

	URL.revokeObjectURL(a.href);

};

function download() {

	let json = getInput()
	downloadToFile(json.text, json.name, 'application/json');
	fileName = null
		    cleanUp()
}

// UPLOAD FILES FROM COMPUTER


async function uploadFiles() {

	let form = document.createElement("form");
	document.body.appendChild(form)
	form.method = 'POST';
	form.enctype = "multipart/data"
	form.action = './uploadFile'

	let input = document.createElement("input");
	input.style.display = "none"
	input.type = "file";
	form.appendChild(input)
	input.setAttribute('oninput', "readFile(this.files[0])")

	input.click()
}

async function readFile(file) {

console.log(file)
	if (file.type === "application/json") {
		const reader = new FileReader();
		reader.addEventListener('load', (event) => {
		    const result = event.target.result;
			document.querySelector('[contenteditable]').innerHTML = result
		  });

		let fileContent = reader.readAsText(file)
		console.log(fileContent)
			    cleanUp()
	} else {
		message.innerHTML = "Upload a json file"
	}
}
