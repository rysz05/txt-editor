const express = require('express');
const app = express();
const path = require('path')
const port = 3003;
var fs = require('fs');

app.get('/', (req, res) => {
  res.sendFile(path.join(__dirname+'/main.html'));
})

app.use(express.static('public'));
app.use(express.json());



// ROUTES FOR HANDLING AXIOS CALLS

app.post("/saveInput", (req, res) => {
	// console.log(req.body)
	let saveToFile = JSON.stringify(req.body)
	fs.writeFile('./savedFiles/' + req.body.name, saveToFile, function(err) {
	    if (err) {
	        console.log(err);
	    }
	});
	res.json(null)
})

app.get("/getSavedFiles", (req, res) => {
	fs.readdir('./savedFiles', (err, files) => {
		if (err) {console.log(err)}
		else {
			res.send(files)
		}
	})
})

app.get("/loadFile/:fileName", (req, res) => {
	fs.readFile('./savedFiles/' + req.params.fileName, 'utf8', (err, f) => {
		if (err) {console.log(err)}
		else {
			console.log(f)
			res.send(f)
		}
	})
})

app.get("/deleteFiles", (req, res) => {

		fs.readdir('./savedFiles', (err, files) => {
			if (err) {console.log(err)}
			else {
				for (let i in files) {
					fs.unlink('./savedFiles/' + files[i], (err) => {
						console.log(err)
					})
				}
			} res.json(null)
		})
})



app.listen(port, () => {
  console.log(`Server started on port: ${port}`)
})
